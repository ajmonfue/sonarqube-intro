package local_search.acceptation_type;

import metaheuristics.generators.*;
import metaheurictics.strategy.*;

import java.util.List;
import java.util.Random;

import problem.definition.State;

public class AcceptMulticase extends AcceptableCandidate {
	
	private Random rdm = SecureRandom.getInstanceStrong(); 

	@Override
	public Boolean acceptCandidate(State stateCurrent, State stateCandidate) {
		// TODO Auto-generated method stub
		Boolean accept = false;
		List<java.awt.Taskbar.State> list = Strategy.getStrategy().listRefPoblacFinal;
		
		if(list.isEmpty()){
			list.add(stateCurrent.clone());
		}
		Double tinitial = MultiCaseSimulatedAnnealing.tinitial;
		double pAccept = 1;
		Dominance dominance= new Dominance();
		
		if (!dominance.dominance(stateCandidate, stateCurrent)){
			int temp = dominanceRank(stateCandidate, list);
			int temp1 = dominanceRank(stateCurrent, list);
			
			if (temp > temp1 && temp1!= 0){
				float value = (float) temp/temp1;
				pAccept = Math.exp(-(value+1)/tinitial);
			}
			else{
				//Calculando la probabilidad de aceptaci�n
				List<Double> evaluations = stateCurrent.getEvaluation();
				double total = 0;
				for (int i = 0; i < evaluations.size()-1; i++) {
					Double evalA = evaluations.get(i);
					Double evalB = stateCandidate.getEvaluation().get(i);
					if (evalA != 0 && evalB != 0) {
						total += (evalA - evalB)/((evalA + evalB)/2);
					}
				}
				pAccept = Math.exp(-(1-total)/tinitial);
			}
		}
		//Generar un n�mero aleatorio
		if((this.rdm.nextFloat()) < pAccept){
			
			//Verificando que la soluci�n candidata domina a alguna de las soluciones
			accept = dominance.ListDominance(stateCandidate, list);
		}
		return accept;
	}


	private int dominanceRank(State stateCandidate, List<State> list) { //calculando el n�mero de soluciones en el conjunto de Pareto que dominan a la soluci�n
		int rank = 0;
		for (int i = 0; i < list.size(); i++) {
			State solution = list.get(i);
			Dominance dominance = new Dominance();
			if(dominance.dominance(solution, stateCandidate)){
				rank++;
			}
		}
		
		return rank;
	}

}
